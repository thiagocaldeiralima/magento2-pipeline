<?php
namespace Deployer;

require 'recipe/magento2.php';

// Project name
set('application', 'bottomline');

// Project repository
set('repository', 'git@bitbucket.org:thiagocaldeiralima/magento2-pipeline.git');

// Hosts

host('dev')
    ->hostname('t1.acaldeira.com.br')
    ->user('ec2-user')
    ->stage('dev')
    ->roles('app')
    ->forwardAgent(false)
    ->set('deploy_path', '/var/www/bottomline')
    ->set('branch', 'develop');

host('prod')
    ->hostname('t1.acaldeira.com.br')
    ->user('ec2-user')
    ->stage('prod')
    ->roles('app')
    ->forwardAgent(false)
    ->set('deploy_path', '/var/www/prodbottomline')
    ->set('branch', 'master');